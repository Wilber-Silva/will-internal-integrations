const BaseLambda = require('./BaseLambda')

class Bunker extends BaseLambda {
    constructor() {
        super({
            name: 'bunker-card-api'
        })
        this.functions = {
            Create: 'Create'
            , Find: 'Find'
            , List: 'List'
        }
        this.key = process.env.AUTH_API_KEY || 'msWcs9miWZiCouVJwLT6of0r4cnPC3a4ELV7fc00'
    }
    async createCard (card) {
        this.setFunctionName(this.functions.Create)
        await this.invoke({
            headers: {
                'x-api-key': this.key
            }
            , body: card
        })
        return this.data
    }
    async find(id) {
        this.setFunctionName(this.functions.Find)
        await this.invoke({
            headers: {
                'x-api-key': this.key
            }
            , pathParameters: {
                id
            }
        })
        return this.data
    }
    async list (queryParams) {
        this.setFunctionName(this.functions.List)
        
        if(queryParams.query) queryParams.query = JSON.stringify(queryParams.query)

        await this.invoke({
            headers: {
                'x-api-key': this.key
            }
            , queryStringParameters: queryParams
        })
        return this.data
    }
}

module.exports = new Bunker()