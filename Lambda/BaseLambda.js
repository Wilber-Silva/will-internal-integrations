let aws
try {
    aws = require('aws-sdk')
}
catch (error) {
    console.error(error)
}


class BaseObject {
    constructor({ name }) {
        aws.config.update({ region: 'us-east-1' })
        this.lambda = new aws.Lambda()
        this.FunctionName = ''
        this.name = name
        this.stage = process.env.STAGE || 'dev'
    }
    setFunctionName (functionName) {
        this.FunctionName = `${this.name}-${this.stage}-${functionName}`
    }
    formatResponse ({ StatusCode, Payload }) {
        return {
            RuntimeStatusCode: StatusCode,
            Response: JSON.parse(Payload)
        }
    } 
    getResponseStatusCode () {
        return this.data.Response.statusCode
    }
    getResponseBody () {
        return JSON.parse(this.data.Response.body)
    }
    getRuntimeStatusCode () {
        return this.data.RuntimeStatusCode
    }
    async invoke (params) {
        if (params.queryStringParameters) JSON.stringify(params.queryStringParameters)
        if (params.body) params.body = JSON.stringify(params.body)
        this.data = await this.lambda.invoke({
            FunctionName: this.FunctionName
            // , InvocationType: 'Event'
            // , LogType: 'Tail'
            , Payload: JSON.stringify(params)
        }).promise()
        console.log(this.data)
        this.data = this.formatResponse(this.data)
    }
}

module.exports = BaseObject