const BaseLambda = require('./BaseLambda')

class Auth extends BaseLambda {
    constructor() {
        super({
            name: 'authenticate-api'
        })
        this.functions = {
            UserCreate: `UserCreate`
            , UserUpdate: `UserUpdate`
            , UserGrantAccess: `UserGrantAccess`
            , UserAuthenticate: `UserAuthenticate`
            , UserRefresh: `UserRefresh`
        }
        this.key = process.env.AUTH_API_KEY
    }
    async createUser (user) {
        this.setFunctionName(this.functions.UserCreate)
        await this.invoke({
            headers: {
                'x-api-key': this.key
            }
            , body: user
        })
        return this.data
    }

    async updateUser (id, user) {
        this.setFunctionName(this.functions.UserUpdate)
        await this.invoke({
            pathParameters: { id }
            , headers: {
                'x-api-key': this.key
            }
            , body: user
        })
        return this.data
    }

    async signin ({ login, password, custom }) {
        this.setFunctionName(this.functions.UserGrantAccess)
        await this.invoke({
            headers: {
                'x-api-key': this.key
            }
            , body: {
                login, password, custom
            }
        })
        return this.data
    }

    async validateAccess ({ token }) {
        this.setFunctionName(this.functions.UserAuthenticate)
        await this.invoke({
            headers: {
                'x-api-key': this.key
            }
            , body: {
                token
            }
        })
        return this.data
    }

    async refreshAccess ({ token }) {
        this.setFunctionName(this.functions.UserRefresh)
        await this.invoke({
            headers: {
                'x-api-key': this.key
            }
            , body: {
                token
            }
        })
        return this.data
    }
}

module.exports = new Auth()