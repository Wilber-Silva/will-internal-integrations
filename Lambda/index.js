const AuthLambda = require('./Auth')
const BunkerCardLambda = require('./BunkerCard')

module.exports = {
    AuthLambda
    , BunkerCardLambda
}