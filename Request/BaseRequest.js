let axios
try {
    axios = require('axios')
}
catch (error) {
    console.error(error)
}


class BaseObject {
    constructor() {
        this.http = axios.create({
            baseUrl: process.env.AUTH_BASE_URL,
            headers: {
                'x-api-key': process.env.AUTH_API_KEY
            }
        })
    }

}

module.exports = BaseObject