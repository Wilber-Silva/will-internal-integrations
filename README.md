# README #

Runtime require nodeJs v12

If use serverless framework in aws-lambda you need to set iam roles

```yml
iamRoleStatements:
    - Effect: 'Allow'
      Action:
        - "logs:CreateLogGroup"
        - "logs:CreateLogStream"
        - "logs:PutLogEvents"
        - "ec2:CreateNetworkInterface"
        - "ec2:DescribeNetworkInterfaces"
        - "ec2:DeleteNetworkInterface"
        - "lambda:InvokeFunction"
        - "lambda:InvokeAsync"
      Resource: "*"
```
